using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class puntsController : MonoBehaviour
{
    int punts;   
    void Start()
    {
        punts = 0;
        this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "No tens punts...";
    }

    public void ActPunts()
    {
        punts += 1;
        this.GetComponent<TextMeshProUGUI>().text = "Punts: " + punts;
        
        
        
        /*if(punts%10 == 0)
        {
            // Fer quelcom per augmentar la velocitat de les salses.
        }*/
    }
    public void ActVides(int vides)
    {
        this.gameObject.transform.GetChild(0).GetComponent<Transform>().localScale= new Vector3(vides,1,1);
    }


}
