using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonaController : MonoBehaviour
{
    public int vel;
    public List<Waypoint> waypoints;
    private Waypoint target;
    // Start is called before the first frame update
    void Start()
    {
        target = waypoints[0];
        Vector3 direccio = (target.waypoint - this.transform.position);
        direccio.Normalize();
        this.GetComponent<Rigidbody2D>().velocity = direccio * vel;
    }

    // Update is called once per frame
    void Update()
    {
        if ((target.waypoint - this.transform.position).magnitude < 0.1)
        {
            print("trobat waypoint");
            waypoints.Remove(target);

            if (waypoints.Count > 0)
            {
                target = waypoints[0];
                Vector3 direccio = (target.waypoint - this.transform.position);
                direccio.Normalize();
                this.GetComponent<Rigidbody2D>().velocity = direccio * vel;
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            }
        }
    }
}
