using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paretController : MonoBehaviour
{
    public AlumneController prota;
    void Start()
    {
        // Aix� �s una subscripci�
        // L'objecte Paret se subscriu a l'event OnAgafarClau
        prota.OnAgafarClau += despetjar; 
    }


    public void despetjar()
    {
        if (!this.GetComponent<BoxCollider2D>().isTrigger)
        {
            this.GetComponent<BoxCollider2D>().isTrigger = true;
            this.GetComponent<SpriteRenderer>().color = Color.green;
        } else
        {
            this.GetComponent<BoxCollider2D>().isTrigger = false;
            this.GetComponent<SpriteRenderer>().color = Color.red;
        }
    }

    private void OnDisable()
    {
        prota.OnAgafarClau -= despetjar;
    }


}
