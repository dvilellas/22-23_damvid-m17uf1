using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{

    public int vel;
    public GameObject posicio;
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if(this.GetComponent<Rigidbody2D>().transform.position.x < -20)
        {
            this.GetComponent<Rigidbody2D>().transform.position = posicio.transform.position;
        }
    }
}
