using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AlumneController : MonoBehaviour
{
    public delegate void AgafarClau();
    public event AgafarClau OnAgafarClau;

    public GameObject gota;
    public int vides;
    public puntsController puntsManager;
    public GameObject puntsController;

    private void Start()
    {
        vides = 3;
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.D))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(8, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-8, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 500));
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject nova = Instantiate(gota);
            nova.GetComponent<Transform>().position = this.gameObject.GetComponent<Transform>().position;
            nova.GetComponent<Rigidbody2D>().velocity = Vector2.right * 4;
            nova.GetComponent<gotaController>().Onbeure += puntsController.GetComponent<puntsController>().ActPunts;
        }

    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "clau")
        {
            this.GetComponent<AudioSource>().Play();
            if(OnAgafarClau != null)
            {
                OnAgafarClau.Invoke();
            }
        }
    }
    
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.transform.tag == "salsa")
        {
            vides--;
            puntsManager.ActVides(vides);
            StartCoroutine(perdreVida());
            if (vides <= 0)
            {
                print("gameover Mel�");
                SceneManager.LoadScene("Desert");
            }
        }
    }

    IEnumerator perdreVida()
    {
        this.GetComponent<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(0.2f);
        this.GetComponent<SpriteRenderer>().color = Color.white;
        yield return new WaitForSeconds(0.2f);
        this.GetComponent<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(0.2f);
        this.GetComponent<SpriteRenderer>().color = Color.white;
    }

}
