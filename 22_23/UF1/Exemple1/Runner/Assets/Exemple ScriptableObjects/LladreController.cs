using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LladreController : MonoBehaviour
{
    // Start is called before the first frame update
    public Posicio ubicacio;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(8, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-8, this.GetComponent<Rigidbody2D>().velocity.y);
        } else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }

        if (Input.GetKey(KeyCode.W))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 8);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -8);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            ubicacio.invisible = !ubicacio.invisible;
        }

        ubicacio.pos = this.GetComponent<Transform>().position;
        
    }
}
