using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TorresController : MonoBehaviour
{
    public GameObject torrePrefab;
    public Torre[] definicions;

    void Start()
    {
        // Posicions aleatories entre 8 i -8 a x, i 4 i -4 a y
        
        foreach ( Torre torre in definicions)
        {
            //print("UNA TORRE"); 
            GameObject t1 = Instantiate(torrePrefab);
            t1.GetComponent<TorreController>().CarregarInfoTorre(torre);
            t1.GetComponent<Transform>().position = new Vector2(Random.Range(-6f, 6f), Random.Range(-4f, 4f));

        }
        //    t1.GetComponent<TorreController>().atk = definicions[0].GetComponent<Torre>().atk;
        //    t1.GetComponent<TorreController>().hp = definicions[0].GetComponent<Torre>().hp;
        //    t1.GetComponent<SpriteRenderer>().color = definicions[0].coloret; 
        //    t1.GetComponent<SpriteRenderer>().sprite = definicions[0].img;
        //    t1.GetComponent<Transform>().position = new Vector2(Random.Range(-6f,6f), Random.Range(-4f,4f));

        GameObject t2 = Instantiate(torrePrefab);
        t2.GetComponent<TorreController>().atk = definicions[1].atk;
        t2.GetComponent<TorreController>().hp = definicions[1].hp;
        t2.GetComponent<TorreController>().nivell = definicions[1].nivell;
        t2.GetComponent<SpriteRenderer>().color = definicions[1].coloret;
        if (definicions[1].img != null)
            t2.GetComponent<SpriteRenderer>().sprite = definicions[1].img;
        t2.GetComponent<Transform>().position = new Vector2(Random.Range(-6f, 6f), Random.Range(-4f, 4f));

    }

  
    void Update()
    {
        
    }
}
