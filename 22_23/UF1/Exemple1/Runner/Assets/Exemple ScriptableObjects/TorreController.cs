using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorreController : MonoBehaviour
{

    public int atk;
    public int hp;
    public int nivell;
    

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CarregarInfoTorre(Torre info)
    {
        hp = info.hp;
        atk = info.atk;
        nivell = info.nivell;
        GetComponent<SpriteRenderer>().color = info.coloret;
        if(info.img!=null)
            GetComponent<SpriteRenderer>().sprite = info.img;

    }
}
