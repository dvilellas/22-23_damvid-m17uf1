using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Torre : ScriptableObject
{
    public int hp;
    public int atk;
    public int nivell;
    public Color coloret;
    public Sprite img;
}
