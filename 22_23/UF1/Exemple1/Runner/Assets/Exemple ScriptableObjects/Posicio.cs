using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Posicio : ScriptableObject
{
    public Vector2 pos;
    public bool invisible;
    public int vel;
}
