using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaController : MonoBehaviour
{
    public Posicio ubicacioLladre;
    public GameObject bala;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!ubicacioLladre.invisible)
        {
            if (ubicacioLladre.pos.x > this.GetComponent<Transform>().position.x)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(2, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-2, this.GetComponent<Rigidbody2D>().velocity.y);
            }

            if (ubicacioLladre.pos.y > this.GetComponent<Transform>().position.y)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 2);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -2);
            }
        } else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
    }
}
