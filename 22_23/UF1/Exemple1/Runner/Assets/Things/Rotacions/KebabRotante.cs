using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KebabRotante : MonoBehaviour
{
    public float spd;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Spin");
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }

    public IEnumerator Spin()
    {
        while (true)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                this.GetComponent<Rigidbody2D>().velocity = this.transform.right * spd;
            }
            else
            {


                this.transform.Rotate(0, 0, 1);
            }
            yield return new WaitForSeconds(0.1f);

        }
    }
}

