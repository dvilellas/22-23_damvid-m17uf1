using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalterChikito : MonoBehaviour
{
    public Transform kebab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = kebab.transform.position - this.transform.position;
        this.transform.up = dir;

        this.GetComponent<Rigidbody2D>().velocity = dir.normalized;

    }
}
