using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalterUI : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void WalterRed()
    {
        this.GetComponent<SpriteRenderer>().color = Color.red;
    }

    public void WalterRed(float f)
    {
        this.GetComponent<SpriteRenderer>().color = new Color(1, 1 - f, 1 - f);
    }

    public void chiki(int p)
    {
        if (p == 0)
        {
            this.transform.localScale = new Vector3(3.6947f, 0.2785009f, 1);
        }else if (p == 1)
        {
            this.transform.localScale = new Vector3(0.1f, 0.2785009f, 1);
        }
    }
}
