using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            for (int i = 0; i < this.transform.childCount; i++)
            {
                if (!this.transform.GetChild(i).gameObject.activeSelf)
                {
                    this.transform.GetChild(i).gameObject.SetActive(true);
                    this.transform.GetChild(i).GetComponent<Rigidbody2D>().velocity = new Vector2(3, 0);
                    this.transform.GetChild(i).position = this.transform.position;
                    break;
                }
            }
        }
    }
}
